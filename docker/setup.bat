@ECHO off

echo load the environment variables

setlocal enableextensions enabledelayedexpansion

if EXIST ".env" (
  for /F "tokens=*" %%I in (.env) do @set %%I
)


echo docker-compose  up -d
docker-compose up -d
echo Waiting a few seconds to let the containers load up.
timeout /t 4 
echo docker ps - Showing the created containers
docker ps
timeout /t 4

echo docker exec app composer install
docker exec app composer install

docker exec db mysql -u root -p"%MYSQL_ROOT_PASSWORD%" -e "show databases;"
docker exec db mysql -u root -p"%MYSQL_ROOT_PASSWORD%" -e "GRANT ALL PRIVILEGES ON *.* TO 'laraveluser'@'%' IDENTIFIED BY 'MSpacile321';"
docker exec db mysql -u root -p"%MYSQL_ROOT_PASSWORD%" -e "GRANT ALL PRIVILEGES ON *.* TO 'laraveluser'@'app.laranet' IDENTIFIED BY 'MSpacile321';"
docker exec db mysql -u root -p"%MYSQL_ROOT_PASSWORD%" -e "FLUSH PRIVILEGES;"
docker exec db mysql -u root -p"%MYSQL_ROOT_PASSWORD%" -e "FLUSH PRIVILEGES;"

echo create the test laravel app
echo docker exec app composer create-project --prefer-dist laravel/laravel ../www 
docker exec app composer create-project --prefer-dist laravel/laravel ../www  
rem docker exec app rsync -vua --delete-after tempapp/ .
rem docker exec app composer require fzaninotto/faker


echo docker exec app php artisan key:generate
docker exec app php artisan key:generate


docker pull node:10
echo docker run -dit -v "%DOCKER_LOCATION%":"%INSTALL_SRC%" -v "%PROJECT_LOCATION%":"%INSTALL_PROJ%" -w %INSTALL_SRC% --name nodeinstaller node
docker run -dit -v "%DOCKER_LOCATION%":"%INSTALL_SRC%" -v "%PROJECT_LOCATION%":"%INSTALL_PROJ%" -w %INSTALL_SRC% --name nodeinstaller node
docker exec nodeinstaller npm i

docker exec nodeinstaller npm run setupEnvFile

echo docker exec app php artisan config:cache
docker exec app php artisan config:cache
echo docker exec app php artisan migrate:install
docker exec app php artisan migrate:install

echo docker exec app rsync -vua /usr/migrations/ /var/www/database/migrations/

docker exec app rsync -vua /usr/migrations/ /var/www/database/migrations/

echo docker exec app php artisan migrate
docker exec app php artisan migrate

echo docker exec app php artisan db:seed
docker exec app php artisan db:seed

echo unsetting configuration values
docker container stop nodeinstaller
docker container rm nodeinstaller
echo setup complete
