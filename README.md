# Environment Setup

For this project I have included a version of my docker containers setup since I am running docker on windows and cant use Valet. I have had a lot of problems with laravels docker set up and for my own sanity I am using a version that has worked for me for the past few years, updated for laravel 7 and php 7.4 of course.


For simplicity I have trimmed down my usual setup to only include PHP, NGINX and a MySql server in the docker-compose file and only included scripts related to automating the startup process. This includes the setup.bat file and the setup_config.js script which does a string replacement for the laravel .env configuration.

I apologize for not including the .sh version as I am unable to test it to make sure it works as expected.

I have included the required database script as a part of the initial set up and it is located in the ./docker/migrations directory for use in the corresponding migration.

# Challenge Code

For this challenge, the code I added is located as follows

```
./app/Http/Controllers/DataControllers/TeamSortController.php
./app/Http/Controllers/PageControllers/TeamController.php
./app/Team.php
./resources/views/teams.blade.php

A route in web.php that is as follows:

Route::get('/', '\App\Http\Controllers\PageControllers\TeamController@index');

The test logic was added into the file provided:
./tests/Unit/PlayersIntegrityTest.php

```

The tests were added to the test class provided.
