<?php

namespace App;

use App\Exceptions\TeamException;

class Team
{
    public $name;
    public $players;
    public $rank = 0;
    public $playerCount = 0;

    public function __construct() {
    	$this->name = $this->generateTeamName();
    	$this->players = [];
    	$this->rank = 0;
    }

    public function addPlayer($player){
    	$playerMissing = true;
    	foreach($this->players as $p){
    		if($p->id == $player->id){
    			$playerMissing = false;
    		}
    	}

    	if($playerMissing){
    	   	$this->players[] = $player;
    	   	$this->rank += $player->ranking;
    	   	$this->playerCount++;
    	}
    }

    private function generateTeamName(){
    	//
    	$faker = \Faker\Factory::create();
    	$domain = $faker->domainWord();

    	if (substr($domain, -1) != 's') {
    		$domain .= "s";
    	}

    	return $faker->lastName() . " " . $domain;
    }
}