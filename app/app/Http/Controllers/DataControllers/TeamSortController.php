<?php
/**
 * @author joel diaz
*/

namespace App\Http\Controllers\DataControllers;
use App\Team;
use App\User;

/**
 * TeamSortController - Data Manipulation Controller
 * This does a lot of data manipulation so I removed this logic from the controllers which handle returning pages to routes.
 */
class TeamSortController
{
    CONST MIN_SIZE = 18;
    CONST MAX_SIZE = 22;


    /**
	 * @method generateTeams
	 * Main method called for generating team data, used to do everything needed
     */
    public function generateTeams(){
    	// get the list of all possible players

        // figure out the limits for team assignments
        $teamCounts = $this->calculateTotalTeams();

        // If you want variable length teams do not set the max size, it will assume default max and fill to that limit
    	// return $this->sortPlayersIntoTeams($teamCounts["totalTeams

        // Return the team data generated
        return $this->putPlayersIntoTeams($teamCounts["totalTeams"]);
    	return $this->putPlayersIntoTeams($teamCounts["totalTeams"], $teamCounts["teamSize"]);
    }

    /**
	 * @method putPlayersIntoTeams
	 * 
	 * This method handles some setup for generating the team data and assigning the first
	 * players which are goalies. We set a single goalie to each team to garuntee that there will
	 * be at least one, then we take the remaining goalies and loop through them the way we would with any other player list.
	 * 
	 * @param int $totalTeams the maximum number of teams
	 * @param int $teamSize the estimated size of each team (default is MAX_SIZE)
	 * 
	 * @return array a list of all the generated teams
     */
    protected function putPlayersIntoTeams($totalTeams, $teamSize=self::MAX_SIZE){
        //For flavor i randomized the order of the players in a rank group to see some variance in assignments
        $usersList = User::where("user_type", 'player')->orderByDesc('ranking')->inRandomOrder()->get();
        $players = $this->getListAsObjectArray($usersList->where('can_play_goalie', 0));
        $goalies = $this->getListAsObjectArray($usersList->where('can_play_goalie', 1));
        $teams = [];

        for($x = 0; $x < $totalTeams; $x++){
            $teams[] = new Team();
        }
        //put the top rated goalies into each new team
        foreach($teams as $team){
            if(count($goalies) > 0){
                $goalie = array_pop($goalies);
                $team->addPlayer($goalie);
            }
        }
        // for the remaining goalies add them as players based on ranks
        $teams = $this->sortPlayersInTeamsByRank($teams, $goalies, $teamSize);
        // for the players add them to teams based on ranks
        $teams = $this->sortPlayersInTeamsByRank($teams, $players, $teamSize);
        return $teams;

    }

    /**
	 * @method sortPlayersInTeamsByRank
	 * Sort through a list of players and teams and assign usign the best match by ranking
	 * @param array $teams the list of teams objects
	 * @param array $players the list of players
	 * @param int $limit the maximum number of players on a team
     */
    private function sortPlayersInTeamsByRank($teams, $players, $limit ){
    	// method used for sorting the teams by ranking
        $rankSort = function ($a, $b) {
            return strcmp($a->rank, $b->rank);
        };
        
        // sort the teams by lowest ranking first
        usort($teams, $rankSort);

        //add players when the player count is larger than 0
        while(count($players) > 0){
            $teamsMaxedOut = 0;
        	// sort the teams before assigning the next set of players
            usort($teams, $rankSort);

            // keep score of the current round of max score and try to fill to match it.
            $maxRank = max(array_column($teams, "rank"));

            // fill players for each team
            foreach($teams as $team){
                // you can also fill until this team is the new max rank, this may lead to some lower rated teams.
                // Leaving it commented out but included for reference
                // while($team->rank < $maxRank && count($players) > 0 && count($team->players) < $limit){

                // only fill when the score is below the current max, there are still players to add, and the team has not reached the max limit
                if($team->rank < $maxRank && count($players) > 0 && count($team->players) < $limit){
                    $player = array_pop($players);
                    $team->addPlayer($player);
                }
            }

            // after each round of filling, make sure all teams are not yet maxxed out (can cause infinite loops)
            foreach($teams as $team){
                if(count($team->players) >= $limit){
                    $teamsMaxedOut++;
                }
            }

            // in case we do max out all teams, break out of this loop
            if($teamsMaxedOut == count($teams)){
                break;
            }

        }
        usort($teams, $rankSort);
        return $teams;
    }

    /**
	 * @method getListAsObjectArray
	 *
     * While I like Laravels Collection dataset, using toArray doesn't preserve the object
     * while using unset deletes the object. At least by putting the objects into an array I can
     * sort as needed and remove objects from the array while keeping the object alive.
     *
     * @return array   a converted array of eloquent model objects
	 */
    private function getListAsObjectArray($list){
        $arr = [];
        foreach($list as $item){
            $arr[] = $item;
        }
        return $arr;
    }

    
    /**
	 * @method calculateTotalTeams
	 *
     * In general we can assume there are some constraints to how we generate team sizes. This handles figuring those counts out
     *
     * @return array   calculated team counts data
	 */
    public function calculateTotalTeams(){
        $usersList = User::where("user_type", 'player')->orderByDesc('ranking')->get();
        $players = $usersList->where('can_play_goalie', 0)->count();
        $goalies = $usersList->where('can_play_goalie', 1)->count();
        $allPlayers = $usersList->count();
        $totalTeams = 0;

        //First lets assume we can have only 1 goalie per team, that will give us an absolute max
        if($goalies % 2 == 0){
            $totalTeams = $goalies;
        }else{
            $totalTeams = $goalies - 1;
        }

        //to start our estimate assume the team size based max teams with a goalie
        $teamSize = floor($allPlayers / $totalTeams);

        //While the team size is less than the minimum or larger than the maximum, adjust until it reaches the right range
        while($teamSize < self::MIN_SIZE){
            $totalTeams -= 2;
            $teamSize = floor($allPlayers / $totalTeams);
        }

        //After processing size, if you have more players than allowed, make some adjustments for max size and total possible teams.
        if($teamSize > self::MAX_SIZE){
        	$teamSize = self::MAX_SIZE;
        	$maxSizedTeams = floor($usersList->count() / $teamSize);
        	if ($maxSizedTeams > $goalies){
		        if($goalies % 2 == 0){
		            $totalTeams = $goalies;
		        }else{
		            $totalTeams = $goalies - 1;
		        }
        	}else{
		        if($maxSizedTeams % 2 == 0){
		            $totalTeams = $maxSizedTeams;
		        }else{
		            $totalTeams = $maxSizedTeams - 1;
		        }
        	}
        }

        //calculate the estimated total if each team has the same number of players
        $estimatedPlayers = $teamSize * $totalTeams;

        // return the estimates for further processing
        return compact('players', 'goalies', 'allPlayers', 'teamSize', 'totalTeams', 'estimatedPlayers');
    }
}