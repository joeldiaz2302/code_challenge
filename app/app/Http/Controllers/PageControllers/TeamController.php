<?php
/**
 * @author joel diaz
*/

namespace App\Http\Controllers\PageControllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\DataControllers\TeamSortController;

/**
 * Page controller for the page to display the dynamically generated teams.
 *
 * Methods all extended based on laravel usage of GET, POST, PUT, etc. methods
 */
class TeamController extends Controller
{
	public function index(){
        $teams = (new TeamSortController)->generateTeams();
		return view('teams', compact('teams'));
	}
}