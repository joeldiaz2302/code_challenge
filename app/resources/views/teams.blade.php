<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .container {
                display: inline-flex;
                width: 95vw;
                margin: auto;
                height: 80vh;
                overflow: auto;
            }

            .table {
                min-width: 365px;
            }

            .header {
                text-transform: capitalize;
                width: 100%;
                font-weight: 600;
                font-size: 16px;
            }

            .line {
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Generated Teams Results
                </div>
                <div class="container">
                    @foreach ($teams as $team)
                        <div class="table">
                            <div class="header">
                               Name: {{ $team->name }} - Total Rank: {{$team->rank}}
                            </div>
                            @foreach ($team->players as $player)
                            <div class="line">
                               {{ $player->first_name }} {{ $player->last_name }}
                            </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>
