<?php

namespace Tests\Unit;

// replaced the source for TestCase parent class because PHPUnit's TestCase does not set up the rest
// of the application needed to test with Eloquent models
// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use App\User;
use App\Http\Controllers\DataControllers\TeamSortController;


class PlayersIntegrityTest extends TestCase
{
    public $counts;

    protected function setUp(): void{
        Parent::setUp();

        $counter = new TeamSortController;
        $players = User::where('user_type', 'player');
        $this->counts = $counter->calculateTotalTeams($players);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGoaliePlayersExist () 
    {
/*
		Check there are players that have can_play_goalie set as 1   
*/
		$result = User::where('user_type', 'player')->where('can_play_goalie', 1)->count();
		$this->assertTrue($result > 1);
	
    }
    
    public function testAtLeastOneGoaliePlayerPerTeam () 
    {
/*
	    calculate how many teams can be made so that there is an even number of teams and they each have between 18-22 players.
	    Then check that there are at least as many players who can play goalie as there are teams
*/
        $goalies = User::where('user_type', 'player')->where('can_play_goalie', 1)->count();
        $size = $this->counts["teamSize"];
        $teams = $this->counts["totalTeams"];

        //There are goalies
        $this->assertTrue($goalies > 0);

        //There are at least the same amount of goalies and teams
        $this->assertTrue($goalies >= $teams);

        //The estimated team size is at least the minimum of 18
        $this->assertTrue($size >= 18);

        //The estimated team size is at most the maximum of 22
        $this->assertTrue($size <= 22);
    }
}